﻿using System;
using System.Collections.Generic;

namespace CRMSB.Models
{
    public partial class Users
    {
        public Users()
        {
            Clients = new HashSet<Clients>();
            ScheduleEvents = new HashSet<ScheduleEvents>();
            Todos = new HashSet<Todos>();
        }

        public long Id { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public int? Access { get; set; }

        public ICollection<Clients> Clients { get; set; }
        public ICollection<ScheduleEvents> ScheduleEvents { get; set; }
        public ICollection<Todos> Todos { get; set; }

        public static Users emptyUser()
        {
            return new Users
            {
                Id = -1, Login = "", Pass = "", FName = "",
                LName = "", Access = -1
            };
        }
    }
}
