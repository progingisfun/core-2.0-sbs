﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRMSB.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using CRMSB.Models;
using Microsoft.EntityFrameworkCore;


namespace CRMSB
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            const string connection = @"Server=localhost;Port=5432;Database=CRM;Password=postgresql;Username=postgres;";
            services.AddDbContext<postgresContext>(options => options.UseNpgsql(connection));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseMvc(routes =>
            {
            routes.MapRoute(
            name: "default",
            template: "{controller=Admin}/{action=Admin_login}/{id?}");
              
        });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                name: "default",
                template: "{controller=User}/{action=User_login}/{id?}");

            });
        }
    }
}