﻿using System;
using System.Collections.Generic;

namespace CRMSB.Models
{
    public partial class ScheduleEvents
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public int? SYear { get; set; }
        public int? SMonth { get; set; }
        public int? SDay { get; set; }
        public int? SHours { get; set; }
        public int? SMins { get; set; }
        public int? EYear { get; set; }
        public int? EMonth { get; set; }
        public int? EDay { get; set; }
        public int? EHours { get; set; }
        public int? EMins { get; set; }
        public string Content { get; set; }

        public Users User { get; set; }
    }
}
