﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NpgsqlTypes;
using CRMSB.Models;
using Microsoft.CodeAnalysis.Text;
using Newtonsoft.Json.Linq;

namespace CRMSB.Controllers
{
    public class UserController: Controller
    {
        private readonly postgresContext _context;

        public UserController(postgresContext context) {
            _context = context;
        }
        public IActionResult Index() {
            String userSession = HttpContext.Session.GetString("User");
            if (userSession == null)
            {
                return RedirectToAction("User_login");
            }else{
                Users users = JsonConvert.DeserializeObject<Users>(userSession);
                ViewData["User"] = users;
                
                IEnumerable<Todos> todos = _context.Todos
                    .FromSql("select * from todos")
                    .Where(t => t.UserId == users.Id).ToList();
                
                IEnumerable<ScheduleEvents> ScEvents = _context.ScheduleEvents
                    .FromSql("select * from schedule_events")
                    .Where(t => t.UserId == users.Id).ToList();

                ViewData["Todos"] = todos;
                ViewData["events"] = ScEvents;
                
                return View();
            }
        }
        public IActionResult User_login()
        {
            String userSession = HttpContext.Session.GetString("User");
            
            if (userSession != null)
            {
                return RedirectToAction("Index");
            }
             return View();
            
        }
        public IActionResult Contact_list()
        {
            String userSession = HttpContext.Session.GetString("User");
            if (userSession == null)
            {
                return RedirectToAction("Index");
            }else{
                Users users = JsonConvert.DeserializeObject<Users>(userSession);
                IEnumerable<Clients> clients = _context.Clients.FromSql("SELECT * FROM clients").Where(c => c.UserId == users.Id).ToList();
                ViewData["User"] = users;
                return View(clients);
            }
            
        }
        public IActionResult User_profile()
        {
            String userSession = HttpContext.Session.GetString("User");
         
            if (userSession == null)
            {
                return RedirectToAction("Index");
            }else{
                Users users = JsonConvert.DeserializeObject<Users>(userSession);
                ViewData["User"] = users;
                return View();
            }
        }
        public IActionResult Add_client()
        {
            String userSession = HttpContext.Session.GetString("User");
         
            if (userSession == null)
            {
                return RedirectToAction("Index");
            }
            else { 
            
                Users users = JsonConvert.DeserializeObject<Users>(userSession);
                ViewData["User"] = users;
                return View();
            }

            
        }
        [HttpPost]
        public IActionResult addclient(Clients clients) {
            _context.Clients.Add(new Clients()
            {
                FName = clients.FName,
                LName = clients.LName,

                PNumber = clients.PNumber,
                Email = clients.Email,
                UserId = clients.UserId,
                City = clients.City,
                
                Country = clients.Country,
                Region = clients.Region
            });
            _context.SaveChanges();
            return RedirectToAction("Add_client");
        }
        [HttpGet]
        public IActionResult Edit_contact(int id)
        {
            String userSession = HttpContext.Session.GetString("User");
            var contact = _context.Clients.FirstOrDefault(c => c.Id == id);
            if (contact == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                Users users = JsonConvert.DeserializeObject<Users>(userSession);
                ViewData["User"] = users;
                return View(contact);
            }
           
        }
        [HttpPost]
        public IActionResult Edit_contact(Clients clients)
        {

            if (ModelState.IsValid)
            {
                _context.Entry(clients).State = EntityState.Modified;
                _context.SaveChanges();
                return RedirectToAction("Contact_list");
            }
            return View(clients);
        }
        [HttpGet]
        public IActionResult delete_contact(int id)
        {
            var model = _context.Clients.FirstOrDefault(c => c.Id == id);
            if (model != null)
            {
                _context.Clients.Remove(model);
                _context.SaveChanges();
                return RedirectToAction("Contact_list");
            }
            else
                return RedirectToAction("Index");

        }
        [HttpPost]
        public IActionResult authorization(Users user)
        {
            Users result = _context.Users.FirstOrDefault(u => u.Login == user.Login && u.Pass == user.Pass);
            if (result != null)
            {
                HttpContext.Session.SetString("User", JsonConvert.SerializeObject(result));
                return RedirectToAction("Index");
            }
            return RedirectToAction("User_login");
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("User");
            return RedirectToAction("User_login");
        }

        [HttpPost]
        public IActionResult Add_todo(Todos todo)
        {
            
            _context.Todos.Add(new Todos()
            {
                Date = todo.Date, Subject = todo.Subject, Type = todo.Type, UserId = todo.UserId
            });
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult Delete_todo(int id)
        {
            var model = _context.Todos.FirstOrDefault(c => c.Id == id);
            
            if (model != null)
            {
                _context.Todos.Remove(model);
                _context.SaveChanges();
            }
            
            return RedirectToAction("Index");
            
        }
        
        
        [HttpPost]
        public IActionResult Search(string value)
        {
            value = value.ToLower();
            Console.WriteLine(value);
            String userSession = HttpContext.Session.GetString("User");
            Users users = JsonConvert.DeserializeObject<Users>(userSession);
            IEnumerable<Clients> clients = _context.Clients.FromSql("SELECT * FROM clients").Where(c => c.UserId == users.Id).ToList();
            List<Clients> clientsRet = new List<Clients>();
            foreach (var client in clients)
            {

                if (client.FName.ToLower().Contains(value)
                    || client.LName.ToLower().Contains(value))
                {
                    clientsRet.Add(client);
                }
            }
            ViewData["User"] = users;
            return View("Contact_list", clientsRet);
        }

        [HttpPost]
        [Route("/User/JsonAuth")]
        public IActionResult JsonAuth([FromBody]dynamic userData)
        {
            JObject jObject = userData;
            String login = jObject.GetValue("login").ToString();
            String password = jObject.GetValue("pass").ToString();
            Console.WriteLine(login);
            Console.WriteLine(password);
            Users result = _context.Users.FirstOrDefault(u => u.Login == login && u.Pass == password);
            Console.WriteLine(result);
            Console.WriteLine("-------------------");
            if (result == null)
            {
                result = Users.emptyUser();
            }
            Console.WriteLine(result.Id);
            return Ok(result);
        }
        [HttpPost]
        [Route("User/JsonClient")]
        public IActionResult JsonClient(String UserId) {

            long id = long.Parse(UserId);
           
            Console.WriteLine(id);
            List<Clients> clients = _context.Clients.FromSql("SELECT * FROM clients").Where(c => c.UserId == id).ToList();
       
         
            return Ok(clients);
        }
        [HttpPost]
        [Route("User/JsonTodos")]
        public IActionResult JsonTodos(String UserId)
        {

            long id = long.Parse(UserId);

            Console.WriteLine(id);
            List<Todos> todos = _context.Todos.FromSql("SELECT * FROM todos").Where(t => t.UserId == id).ToList();


            return Ok(todos);
        }
        [HttpPost]
        [Route("User/JsonTodosDelete")]
        public IActionResult JsonTodosDelete (String TodoId)
        {

            long id = long.Parse(TodoId);

            Console.WriteLine(id);
            var model = _context.Todos.FirstOrDefault(c => c.Id == id);

            if (model != null)
            {
                _context.Todos.Remove(model);
                _context.SaveChanges();
            }

            return Ok();
        }
        [HttpPost]
        [Route ("User/JsonTodosAdd")]
        public IActionResult JsonTodosAdd(String date,String subject,String type,String userId)
        {
            long id = long.Parse(userId);

            _context.Todos.Add(new Todos()
            {
                Date = date,
                Subject = subject,
                Type = type,
                UserId = id
            });
            _context.SaveChanges();
            return Ok();
        }
        [HttpPost]
        [Route("User/JsonClientAdd")]
        public IActionResult JsonClientAdd(String fname,String lname,String pnumber,String email,String country,
            String city,String region,String userId)
        {
            long uid = long.Parse(userId);
            _context.Clients.Add(new Clients()
            {
                
                FName = fname,
                LName = lname,

                PNumber = pnumber,
                Email = email,
                UserId = uid,
                City = city,

                Country = country,
                Region = region
            });
            _context.SaveChanges();
            return Ok();
        }
        [HttpPost]
        [Route("User/JsonClientEdit")]
        public IActionResult JsonClientEdit([FromBody]dynamic client)
        {
            JObject jObject = new JObject(client);
            long id = long.Parse(jObject.GetValue("clientId").ToString());
            var contact = _context.Clients.FirstOrDefault(c => c.Id == id);

            return Ok(contact);
            

        }
        [HttpPost]
        [Route("User/JsonClientChange")]
        public IActionResult JsonClientChange ([FromBody] dynamic clientData)
        {
            JObject jobject = new JObject(clientData);
            long id = long.Parse(jobject.GetValue("clientId").ToString());
            var client = _context.Clients.Where(c => c.Id == id).FirstOrDefault();


            client.FName = jobject.GetValue("fname").ToString();
            client.LName = jobject.GetValue("lname").ToString();
            client.Country = jobject.GetValue("country").ToString();
            client.City = jobject.GetValue("city").ToString();
            client.Region = jobject.GetValue("region").ToString();
            client.PNumber = jobject.GetValue("number").ToString();
            client.Email = jobject.GetValue("email").ToString();
            client.UserId = long.Parse(jobject.GetValue("userId").ToString());
            _context.Entry(client).State = EntityState.Modified;
            _context.SaveChanges();
            return Ok();
        }
        [HttpPost]
        [Route("User/JsonClientDelete")]
        public IActionResult JsonClientDelete(String ClientId)
        {

            long id = long.Parse(ClientId);

            Console.WriteLine(id);
            var model = _context.Clients.FirstOrDefault(c => c.Id == id);

            if (model != null)
            {
                _context.Clients.Remove(model);
                _context.SaveChanges();
            }

            return Ok();
        }
    }
}
