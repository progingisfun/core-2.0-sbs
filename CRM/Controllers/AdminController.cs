using System;
using System.Linq;
using System.Net;
using CRMSB.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NpgsqlTypes;
using System.Collections.Generic;


namespace CRMSB.Controllers
{
    public class AdminController : Controller
    {

        private readonly postgresContext _context;

        public AdminController(postgresContext context)
        {
            _context = context;
        }
        // GET
        public IActionResult Index()
        {
            String adminSession = HttpContext.Session.GetString("Admin");
            if (adminSession == null)
            {
                return RedirectToAction("Admin_login");
            }
            return View();
        }
        public IActionResult Admin_login()
        {
            String adminSession = HttpContext.Session.GetString("Admin");
            if (adminSession != null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }
        public IActionResult Add_user()
        {
            String adminSession = HttpContext.Session.GetString("Admin");
            if (adminSession != null)
            {
                return View();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Edit_user(int id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user == null) {
                return RedirectToAction("Index");
            }
            return View(user);
        }
        [HttpPost]
        public IActionResult Edit_user(Users user) {

            if (ModelState.IsValid) {
                _context.Entry(user).State = EntityState.Modified;
                _context.SaveChanges();
                return RedirectToAction("Users_list");
            }
            return View(user);
        }
        public IActionResult Admin_profile()
        {
            String adminSession = HttpContext.Session.GetString("Admin");
            if (adminSession == null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }
        public IActionResult Users_list()
        {
            String adminSession = HttpContext.Session.GetString("Admin");
            if (adminSession == null)
            {
                return RedirectToAction("Index");
            }

            var model =  _context.Users.ToList();
            if (model == null) {
                return RedirectToAction("Index");
            }
            return View(model);

        }
        
        [HttpPost]
        public IActionResult auth(Admins admin)
        {
            Admins result = _context.Admins.FirstOrDefault(a => a.Login == admin.Login && a.Pass == admin.Pass);
            if (result != null)
            {
                HttpContext.Session.SetString("Admin", JsonConvert.SerializeObject(result));
                return RedirectToAction("Index");
            }
            return RedirectToAction("Admin_login");
        }
        [HttpPost]
        public IActionResult Registration(Users users)
        {

            _context.Users.Add(new Users { FName = users.FName, LName = users.LName, Login = users.Login, Pass = users.Pass, Access = users.Access});
            _context.SaveChanges();
            return RedirectToAction("Add_user");
        }
        public IActionResult Logout() 
        {
            HttpContext.Session.Remove("Admin");
            return RedirectToAction("Admin_login");
        }
      
        [HttpGet]
        public IActionResult Remove(int id) {
            var model = _context.Users.FirstOrDefault(u => u.Id == id);
            if (model != null)
            {
                _context.Users.Remove(model);
                _context.SaveChanges();
                return RedirectToAction("Users_list");
            }
            else
                return RedirectToAction("Index");

        }
    }
    
}
