﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CRMSB.Models
{
    public partial class postgresContext : DbContext
    {
        public postgresContext()
        {
        }

        public postgresContext(DbContextOptions<postgresContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admins> Admins { get; set; }
        public virtual DbSet<Clients> Clients { get; set; }
        public virtual DbSet<ScheduleEvents> ScheduleEvents { get; set; }
        public virtual DbSet<Todos> Todos { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=localhost;Database=postgres;Username=postgres;Password=killbill");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admins>(entity =>
            {
                entity.ToTable("admins");

                entity.HasIndex(e => e.Login)
                    .HasName("admins_login_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasColumnName("login")
                    .HasMaxLength(20);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(20);

                entity.Property(e => e.Pass)
                    .HasColumnName("pass")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Clients>(entity =>
            {
                entity.ToTable("clients");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(20);

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasMaxLength(20);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(60);

                entity.Property(e => e.FName)
                    .HasColumnName("f_name")
                    .HasMaxLength(20);

                entity.Property(e => e.LName)
                    .HasColumnName("l_name")
                    .HasMaxLength(20);

                entity.Property(e => e.PNumber)
                    .HasColumnName("p_number")
                    .HasMaxLength(15);

                entity.Property(e => e.Region)
                    .HasColumnName("region")
                    .HasMaxLength(30);

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .ValueGeneratedOnAdd();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Clients)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("clients_users_id_fk");
            });

            modelBuilder.Entity<ScheduleEvents>(entity =>
            {
                entity.ToTable("schedule_events");

                entity.HasIndex(e => e.Id)
                    .HasName("schedule_events_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Content)
                    .HasColumnName("content")
                    .HasMaxLength(100);

                entity.Property(e => e.EDay).HasColumnName("e_day");

                entity.Property(e => e.EHours).HasColumnName("e_hours");

                entity.Property(e => e.EMins).HasColumnName("e_mins");

                entity.Property(e => e.EMonth).HasColumnName("e_month");

                entity.Property(e => e.EYear).HasColumnName("e_year");

                entity.Property(e => e.SDay).HasColumnName("s_day");

                entity.Property(e => e.SHours).HasColumnName("s_hours");

                entity.Property(e => e.SMins).HasColumnName("s_mins");

                entity.Property(e => e.SMonth).HasColumnName("s_month");

                entity.Property(e => e.SYear).HasColumnName("s_year");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ScheduleEvents)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("schedule_events_users_id_fk");
            });

            modelBuilder.Entity<Todos>(entity =>
            {
                entity.ToTable("todos");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasMaxLength(15);

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasMaxLength(50);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(30);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Todos)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("todos_users_id_fk");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.Login)
                    .HasName("users_login_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Access).HasColumnName("access");

                entity.Property(e => e.FName)
                    .HasColumnName("f_name")
                    .HasMaxLength(20);

                entity.Property(e => e.LName)
                    .HasColumnName("l_name")
                    .HasMaxLength(20);

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasColumnName("login")
                    .HasMaxLength(20);

                entity.Property(e => e.Pass)
                    .HasColumnName("pass")
                    .HasMaxLength(20);
            });

            modelBuilder.HasSequence("schedule_events_user_id_seq");

            modelBuilder.HasSequence("todos_user_id_seq");
        }
    }
}
