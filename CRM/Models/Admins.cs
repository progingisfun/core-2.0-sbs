﻿using System;
using System.Collections.Generic;

namespace CRMSB.Models
{
    public partial class Admins
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public string Name { get; set; }
    }
}
