﻿using System;
using System.Collections.Generic;

namespace CRMSB.Models
{
    public partial class Clients
    {
        public long Id { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PNumber { get; set; }
        public string Email { get; set; }
        public long UserId { get; set; }

        public Users User { get; set; }
    }
}
