﻿using System;
using System.Collections.Generic;

namespace CRMSB.Models
{
    public partial class Todos
    {
        public long Id { get; set; }
        public string Date { get; set; }
        public string Subject { get; set; }
        public string Type { get; set; }
        public long UserId { get; set; }

        public Users User { get; set; }
    }
}
